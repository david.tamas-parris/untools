#' Plot United Nations Refugee and Asylum Time Series
#'
#' @param x A prepared data frame or data table with annual dyadic flows
#' of affected populations. Most likely produced from \code{unref()} using \code{sum_groups = TRUE}, but
#' may be generated from other means with the proper column headings and
#' structure; see details for additional information.
#' @param ... List of arguments passed to default plotting device:
#' \describe{
#' \item{country}{Character string of length one indicating ISO3 character code for country of 
#' interest. Defaults to \code{"USA"}.}
#' \item{yr}{Numeric vector of length two indicating the range of years to subset the dataset. 
#' Defaults to \code{min} and \code{max} of the input data frame.}
#' \item{mode}{A character string indicating plot mode. May be either \code{'out'}
#' (migration out-flows from specified country) or \code{'in'} (migration in-flows
#' to specified country). Default mode is \code{'in'}.}
#' \item{poplabel}{A character string for plot labeling describing the populations summarized using 
#' \code{unref(groups=c('asylum', 'ref'))}. Defaults to \code{'Migrant'}.}
#' }
#' @return Returns a plotting device of class \code{ggplot2}.
#' @export
#'
plot.refts<-function(x, ...) {
  args <- list(...)
  country <- ifelse(is.null(args$country), 'USA', args$country)
  yr<-if(is.null(args$yr)) c(min(x$year), max(x$year)) else c(args$yr[1],args$yr[2])
  mode<-ifelse(is.null(args$mode), 'in', args$mode)
  poplabel<-ifelse(is.null(args$poplabel), 'Migrant', args$poplabel)
  
  if(mode=='in'){
  origin.countries<-x[destination_iso==country & year==yr[2]][order(-value)][1:5][value>0,as.character(origin)]
  dat<-x[destination_iso==country & origin %in% origin.countries]
  dat$origin <- factor(dat$origin, levels = origin.countries)

  plot<-ggplot2::ggplot(dat, ggplot2::aes(x=year, y=value, group=origin))+
    ggplot2::geom_line(ggplot2::aes(color=origin), size=line.size, alpha=line.alpha)+
    ggplot2::geom_point(ggplot2::aes(color=origin), size=point.size, alpha=point.alpha)+
    ggplot2::labs(title=paste(poplabel,'In-Flows to',dat$destination[1],sep=' '),
         subtitle = paste('For The Top',length(unique(levels(dat$origin))),'Origin Countries During',yr[1],'-',yr[2], sep = ' '),
         x='',
         y='')+
    ggplot2::scale_color_manual(values=palette[1:length(unique(dat$origin))],
                       name='')+
    ggplot2::scale_y_continuous(labels=scales::comma)+
    ggplot2::scale_x_continuous(breaks=seq(yr[1],yr[2],5))+
    ggplot2::theme_minimal()+
    lab.settings
  }

  if(mode=='out'){
    destination.countries<-x[origin_iso==country & year==yr[2]][order(-value)][1:5][value>0,as.character(destination)]
    dat<-x[origin_iso==country & destination %in% destination.countries]
    dat$destination <- factor(dat$destination, levels = destination.countries)

    plot<-ggplot2::ggplot(dat, ggplot2::aes(x=year, y=value, group=destination))+
      ggplot2::geom_line(ggplot2::aes(color=destination), size=line.size, alpha=line.alpha)+
      ggplot2::geom_point(ggplot2::aes(color=destination), size=point.size, alpha=point.alpha)+
      ggplot2::labs(title=paste(poplabel,'Out-Flows From',dat$origin[1],sep=' '),
           subtitle = paste('For The Top',length(unique(levels(dat$destination))),'Destination Countries During',yr[1],'-',yr[2], sep = ' '),
           x='',
           y='')+
      ggplot2::scale_color_manual(values=palette[1:length(unique(dat$destination))],
                         name='')+
      ggplot2::scale_y_continuous(labels=scales::comma)+
      ggplot2::scale_x_continuous(breaks=seq(yr[1],yr[2],5))+
      ggplot2::theme_minimal()+
      lab.settings
  }

  print(plot)
  return(plot)
}
