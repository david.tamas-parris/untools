line.size=1.5
point.size=2
palette<-c("#266197", "#82dc59", "#a90aa1", "#6dc5dd", "#794a98", "#57ecc0")
line.alpha=0.75
point.alpha=0.75
lab.settings<-ggplot2::theme(axis.text.x = ggplot2::element_text(angle = 15, hjust = 1))

