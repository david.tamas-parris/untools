# untools 0.1.0

## Package Administration and Information

* Added a `NEWS.md` file to track changes to the package.

* Added a `README.md` file for basic instructions.

* Developed package logo

* Embedded vignette *Exploring United Nations Refugee & Asylum Data With the untools Package*

* Embedded vignette *Exploring United Nations Migrant Stock Data With the untools Package*

## Technical Changes

* Embedded raw code for generating country code master list.

* Fixed bug with where `plot.stocks()` was stripping object of `class()` information.

* Removed `countrycode` dependencies from `getunstock17()` and `unref()` functions.

* Added `getunref()` function which downloads Populations of Concern time series dataset directly from the United Nations website.

* Developed `.gitlab-ci.yml` for remote package testing and hosting package reference and vignette materials.