#' Prepare UNHCR Time Series Data
#'
#' @param x A data frame or data table of raw United Nations High Commisioner of Refugees annual time series data.
#' @param groups A character vector of one or more of the following populations \code{asylum} (asylum seekers),
#' \code{idp} (internally displaced persons), \code{other} (other people), \code{ref} (refugees),
#' \code{ridp} (returned internally displaced persons), \code{rref} (returned refugees),
#' or \code{state} (stateless persons). Defaults to c('ref','asy').
#' @param range A numeric vector of length 2 representing start year and end year of interest. Defaults to c(2000, 2017).
#' @param wide A logical switch indicating whether the return object is in wide or long (default) format.
#' @param sum A logical switch indicating whether the return object should sum populations across groups (default)
#' or maintain column with separate groups.
#' @return A prepared dyadic data frame of United Nations High Commisioner of Refugees
#' raw data summarized by specified groups of migrants with ISO3c country codes.
#' @export
unref<-function(x,
                groups=c('asylum','idp','other','ref','ridp','rref','returnees', 'stateless'),
                range=c(1951,2017),
                wide=FALSE,
                sum=FALSE){

names(x)<-c('year','destination','origin','type','value')
x[value=='*',value:='0'][,value:=as.numeric(value)]
x[,type:=factor(type)]
levels(x$type)<-c('asylum','idp','other','ref','ridp','rref','returnees', 'stateless', 'stateless')
x<-x[,.(value=sum(value)), .(destination,origin,type,year)]
suppressWarnings(x[,origin_iso:=countrycode::countrycode(origin, origin='country.name', destination='iso3c')])
suppressWarnings(x[,destination_iso:=countrycode::countrycode(destination, origin='country.name', destination='iso3c')])
x[origin=='Central African Rep.',origin_iso:='CAF'][destination=='Central African Rep.',destination_iso:='CAF']
x[origin=='Tibetan',origin_iso:='TBT'][destination=='Tibetan',destination_iso:='TBT']
x[destination=='Bosnia and Herzegonia',destination_iso:='BIH']

x<-x[year %in% seq(range[1],range[2],1)]
x<-x[type %in% groups]

setcolorder(x, c('origin_iso','destination_iso','type','year','value','origin','destination'))
x[,type:=factor(type)]

if(sum==TRUE){
x<-x[,.(value=sum(value, na.rm = TRUE)), .(origin_iso,destination_iso,year,origin,destination)]
}

if(wide==TRUE){
  x<-data.table::dcast(x, ...~year)
  cols<-names(x)[(dim(x)[2]-length(seq(range[1],range[2],1))+1):dim(x)[2]]
  x[, (cols) := lapply(.SD, function(x){x[is.na(x)] <- 0; x}), .SDcols = cols]
}

return(x)
}
