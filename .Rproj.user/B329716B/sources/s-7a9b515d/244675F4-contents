#' Plot United Nations Outflows or Inflows
#'
#' @param x A prepared data frame or data table with annual dyadic flows
#' of affected populations. Most likely produced from \code{unref()} using \code{sum = TRUE}, but
#' may be generated from other means with the proper column headings and
#' structure; see details for additional information.
#' @param country A character string with an ISO3 country character code. Defaults to 'USA'.
#' @param yr A 4 digit numeric year. Defaults to the maximum year in the input dataset.
#' @param mode A character string indicating plot mode. Can be either \code{'out'}
#' (migration out-flows from specified country) or \code{'in'} (migration in-flows
#' to specified country). Default mode is \code{'in'}.
#'
#' @return Returns a plotting device of object \code{ggplot()}.
#' @export
#'
plotuninout<-function(x, country='USA', yr=max(x$year), mode='in'){
x<-data.table::setDT(x)
if(mode=='out'){
dat<-x[origin_iso==country & year==yr][order(-value)][1:8][value>0]
dat$destination <- factor(dat$destination, levels = dat$destination[order(-dat$value)])
plot<-ggplot2::ggplot(dat, ggplot2::aes(x=destination, y=value))+
  ggplot2::geom_bar(stat="identity", fill=palette[1])+
  ggplot2::labs(title=paste('Migration Out-Flows From',dat$origin[1],sep=' '),
       subtitle = paste('For The Top',nrow(dat),'Destination Countries During', yr, sep = ' '),
       x='Destination Country',
       y='')+
  ggplot2::scale_y_continuous(labels=scales::comma)+
  ggplot2::theme_minimal()+
  lab.settings
}

if(mode=='in'){
    dat<-x[destination_iso==country & year==yr][order(-value)][1:8][value>0]
    dat$origin <- factor(dat$origin, levels = dat$origin[order(-dat$value)])
    plot<-ggplot2::ggplot(dat, ggplot2::aes(x=origin, y=value))+
      ggplot2::geom_bar(stat="identity", fill=palette[1])+
      ggplot2::labs(title=paste('Migration In-Flows To',dat$destination[1],sep=' '),
           subtitle = paste('For The Top',nrow(dat),'Countries of Origin During', yr, sep = ' '),
           x='Origin Country',
           y='')+
      ggplot2::scale_y_continuous(labels=scales::comma)+
      ggplot2::theme_minimal()+
      lab.settings
}

print(plot)
return(plot)

}
