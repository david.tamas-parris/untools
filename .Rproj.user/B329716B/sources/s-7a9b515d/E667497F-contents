#' Plot United Nations Refugee and Asylum Time Series
#'
#' @param x A prepared data frame or data table with annual dyadic flows
#' of affected populations. Most likely produced from \code{unref()} using \code{sum_groups = TRUE}, but
#' may be generated from other means with the proper column headings and
#' structure; see details for additional information.
#' @param country A character string with an ISO3 country character code. Defaults to 'USA'.
#' @param yrs A numeric vector of length 2 with starting and ending year of interest. Defaults to \code{c(min(x$year),max(x$year))}.
#' @param mode A character string indicating plot mode. Can be either \code{'out'}
#' (migration out-flows from specified country) or \code{'in'} (migration in-flows
#' to specified country). Default mode is \code{'in'}.
#' @param poplabel A character string for plot labeling describing the populations summarized using
#' \code{unref(groups=c('asylum', 'ref'))}. Defaults to \code{'Migrant'}.
#'
#' @return Returns a plotting device of object \code{ggplot()}.
#' @export
#'
plot.refts<-function(x, country='USA',yrs=c(min(x$year),max(x$year)), mode='in', poplabel='Migrant'){
  x<-data.table::setDT(x)
  if(mode=='in'){
  origin.countries<-x[destination_iso==country & year==yrs[2]][order(-value)][1:5][value>0,as.character(origin)]
  dat<-x[destination_iso==country & origin %in% origin.countries]
  dat$origin <- factor(dat$origin, levels = origin.countries)

  plot<-ggplot2::ggplot(dat, ggplot2::aes(x=year, y=value, group=origin))+
    ggplot2::geom_line(ggplot2::aes(color=origin), size=line.size, alpha=line.alpha)+
    ggplot2::geom_point(ggplot2::aes(color=origin), size=point.size, alpha=point.alpha)+
    ggplot2::labs(title=paste(poplabel,'In-Flows to',dat$destination[1],sep=' '),
         subtitle = paste('For The Top',length(unique(levels(dat$origin))),'Origin Countries During',yrs[1],'-',yrs[2], sep = ' '),
         x='',
         y='')+
    ggplot2::scale_color_manual(values=palette[1:length(unique(dat$origin))],
                       name='')+
    ggplot2::scale_y_continuous(labels=scales::comma)+
    ggplot2::scale_x_continuous(breaks=seq(yrs[1],yrs[2],5))+
    ggplot2::theme_minimal()+
    lab.settings
  }

  if(mode=='out'){
    cat('hello')
    destination.countries<-x[origin_iso==country & year==yrs[2]][order(-value)][1:5][value>0,as.character(destination)]
    dat<-x[origin_iso==country & destination %in% destination.countries]
    dat$destination <- factor(dat$destination, levels = destination.countries)

    plot<-ggplot2::ggplot(dat, ggplot2::aes(x=year, y=value, group=destination))+
      ggplot2::geom_line(ggplot2::aes(color=destination), size=line.size, alpha=line.alpha)+
      ggplot2::geom_point(ggplot2::aes(color=destination), size=point.size, alpha=point.alpha)+
      ggplot2::labs(title=paste(poplabel,'Out-Flows From',dat$origin[1],sep=' '),
           subtitle = paste('For The Top',length(unique(levels(dat$destination))),'Destination Countries During',yrs[1],'-',yrs[2], sep = ' '),
           x='',
           y='')+
      ggplot2::scale_color_manual(values=palette[1:length(unique(dat$destination))],
                         name='')+
      ggplot2::scale_y_continuous(labels=scales::comma)+
      ggplot2::scale_x_continuous(breaks=seq(yrs[1],yrs[2],5))+
      ggplot2::theme_minimal()+
      lab.settings
  }

  print(plot)
  return(plot)
}
