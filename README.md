
<!-- README.md is generated from README.Rmd. Please edit that file -->

# untools <img src='man/figures/logo.png' align="right" width="120" />

The `untools` package provides a suite of tools to facilitate acquiring,
processing, and visualizing [United Nations Persons of
Concern](https://www.unhcr.org/ph/persons-concern-unhcr) and [Migrant
Stock](https://www.un.org/en/development/desa/population/migration/data/estimates2/estimates17.asp)
datasets.

This welcome page presents a brief introduction to the `untools`
package. For more detailed information and reference materials for
`untools` please refer to:

  - [*untools* Reference
    Manual](https://dante-sttr.gitlab.io/untools/index.html)
  - [Exploring United Nations Refugee & Asylum Data With the *untools*
    Package](https://dante-sttr.gitlab.io/untools/articles/explore-unhcr.html)
  - [Exploring United Nations Migrant Stock Data With the *untools*
    Package](https://dante-sttr.gitlab.io/untools/articles/explore-unstocks.html)
  - [Creating Georectified Maps of Refugee and Asylum
    Flows](https://dante-sttr.gitlab.io/untools/articles/geo-unref.html)

# Installation

You can install the released version of untools from
[GitLab](https://https://gitlab.com/dante-sttr/untools) with:

``` r
devtools::install_gitlab("/dante-sttr/untools")
```

The following presents a brief introduction to the `untools` package.
For more detailed information, reference materials, and vignettes for
`untools` please refer to:

# Usage

Read in the United Nations Migrant Stock (2017 Revision) data with the
`getunstock17` function. This function will automatically parse the
spreadsheet and provide simple formatting and processing.

``` r
library(untools)

stocks17<-getunstock17()
```

<table>

<thead>

<tr>

<th style="text-align:left;">

host\_iso

</th>

<th style="text-align:left;">

stock\_iso

</th>

<th style="text-align:right;">

year

</th>

<th style="text-align:right;">

value

</th>

<th style="text-align:left;">

host

</th>

<th style="text-align:left;">

stock

</th>

</tr>

</thead>

<tbody>

<tr>

<td style="text-align:left;">

ALB

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

1990

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:left;">

Albania

</td>

<td style="text-align:left;">

Afghanistan

</td>

</tr>

<tr>

<td style="text-align:left;">

ALB

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

1995

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:left;">

Albania

</td>

<td style="text-align:left;">

Afghanistan

</td>

</tr>

<tr>

<td style="text-align:left;">

ALB

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

2000

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:left;">

Albania

</td>

<td style="text-align:left;">

Afghanistan

</td>

</tr>

<tr>

<td style="text-align:left;">

ALB

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

2005

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:left;">

Albania

</td>

<td style="text-align:left;">

Afghanistan

</td>

</tr>

<tr>

<td style="text-align:left;">

ALB

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

2010

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:left;">

Albania

</td>

<td style="text-align:left;">

Afghanistan

</td>

</tr>

<tr>

<td style="text-align:left;">

ALB

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

2015

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:left;">

Albania

</td>

<td style="text-align:left;">

Afghanistan

</td>

</tr>

<tr>

<td style="text-align:left;">

ALB

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

2017

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:left;">

Albania

</td>

<td style="text-align:left;">

Afghanistan

</td>

</tr>

<tr>

<td style="text-align:left;">

DZA

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

1990

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:left;">

Algeria

</td>

<td style="text-align:left;">

Afghanistan

</td>

</tr>

<tr>

<td style="text-align:left;">

DZA

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

1995

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:left;">

Algeria

</td>

<td style="text-align:left;">

Afghanistan

</td>

</tr>

<tr>

<td style="text-align:left;">

DZA

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

2000

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:left;">

Algeria

</td>

<td style="text-align:left;">

Afghanistan

</td>

</tr>

</tbody>

</table>

Plot a time series of the top migrant stock populations in the United
States using the ISO3 character code.

``` r
usa.stocks.ts<-plot(stocks17, country = "USA")
```

<img src="man/figures/README-unnamed-chunk-3-1.png" width="100%" />

Or view a barplot of only the year
2000.

``` r
usa.stocks.static<-plot(stocks17, country = "USA", mode = 'static', yr=2005)
```

<img src="man/figures/README-unnamed-chunk-4-1.png" width="100%" />
